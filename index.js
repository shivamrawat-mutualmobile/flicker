/**
 * @format
 */

import {AppRegistry} from 'react-native';
import App from './app/app';
import {name as appName} from './app.json';

let RootComponent  = App 

AppRegistry.registerComponent(appName, () => RootComponent);
