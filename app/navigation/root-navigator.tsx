import React from 'react';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import {PhotoListScreen} from '../screens/photo-list-screen/index';
import {PhotoDetailScreen} from '../screens/photo-detail-screen/index';
import {IntroductionScreen} from '../screens/introduction-screen/index';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import {
  PhotoListRowProps,
  PhotoDetailsInterface,
} from '../components/image-search';
export type RootStackParamList = {
  PhotoListScreen: undefined;
  PhotoDetailScreen: PhotoListRowProps & {
    photo: PhotoDetailsInterface;
  };
  IntroductionScreen: undefined;
};
//const Stack = createStackNavigator<RootStackParamList>();

const Stack = createNativeStackNavigator<RootStackParamList>();

const RootStack = () => (
  <Stack.Navigator
    screenOptions={{
      headerShown: false,
      gestureEnabled: true,
      stackAnimation: 'fade',
    }}>
    <Stack.Screen name="IntroductionScreen" component={IntroductionScreen} />
    <Stack.Screen name="PhotoListScreen" component={PhotoListScreen} />
    <Stack.Screen name="PhotoDetailScreen" component={PhotoDetailScreen} />
  </Stack.Navigator>
);

interface RootNavigationProps {
  theme: ReactNativePaper.Theme;
}
export const RootStackNavigation: React.FC<RootNavigationProps> = ({theme}) => {
  return (
    <NavigationContainer theme={theme}>
      <RootStack />
    </NavigationContainer>
  );
};
