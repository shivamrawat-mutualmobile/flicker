import React, {useEffect, useState} from 'react';
import {RootStackNavigation} from './navigation/root-navigator';
import {RootStoreProvider} from './model';
import {setupRootStore} from './model';
import {DefaultTheme, Provider as PaperProvider} from 'react-native-paper';
import {getSnapshot} from 'mobx-state-tree';
import {observer} from 'mobx-react-lite';
import {enableScreens} from 'react-native-screens';
enableScreens(true);
const App: React.FC = observer(() => {
  const [RootStoreValue, setRootStore] = useState();

  async function setRootStoreFunc() {
    let data = await setupRootStore();
    setRootStore(data);
  }
  useEffect(() => {
    setRootStoreFunc();
  }, []);
  return (
    <RootStoreProvider value={RootStoreValue}>
      <PaperProvider
        theme={
          RootStoreValue ? getSnapshot(RootStoreValue.theme) : DefaultTheme
        }>
        <RootStackNavigation
          theme={
            RootStoreValue ? getSnapshot(RootStoreValue.theme) : DefaultTheme
          }
        />
      </PaperProvider>
    </RootStoreProvider>
  );
});

export default App;
