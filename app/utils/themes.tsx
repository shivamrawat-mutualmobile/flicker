import {DarkTheme, configureFonts} from 'react-native-paper';
const fontConfig = {
  android: {
    regular: {
      fontFamily: 'Metropolis-Bold',
      fontWeight: 'normal',
    },
    medium: {
      fontFamily: 'Metropolis-Bold',
      fontWeight: 'normal',
    },
    light: {
      fontFamily: 'Metropolis-Light',
      fontWeight: 'normal',
    },
    thin: {
      fontFamily: 'Metropolis-Light',
      fontWeight: 'normal',
    },
  },
};
const DarkThemeFonts = {
  ...DarkTheme,
  fonts: configureFonts(fontConfig),
};
export default DarkThemeFonts;
