import AsyncStorage from '@react-native-async-storage/async-storage';
export async function saveData(key: string, value: string): Promise<boolean> {
  try {
    await AsyncStorage.setItem(key, value);
    return true;
  } catch {
    return false;
  }
}
export async function loadData(key: string): Promise<any | null> {
  try {
    const data = await AsyncStorage.getItem(key);
    if (data != null) {
      return JSON.parse(data);
    } else {
      return null;
    }
  } catch {
    return null;
  }
}
