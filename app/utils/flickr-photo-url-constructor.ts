export function getPhotoUrl(
  server: string,
  id: string,
  secret: string,
  size: string
) {
  return `https://live.staticflickr.com/${server}/${id}_${secret}_${size}.jpg`;
}
