import {types} from 'mobx-state-tree';

const font = types.model({
  fontFamily: types.string,
  fontWeight: types.optional(
    types.enumeration('fontWeight', [
      'normal',
      'bold',
      '100',
      '200',
      '300',
      '400',
      '500',
      '600',
      '700',
      '800',
      '900',
    ]),
    'normal'
  ),
});
const fonts = types.model({
  regular: font,
  medium: font,
  light: font,
  thin: font,
});
const colorStore = types.model({
  primary: types.string,
  background: types.string,
  surface: types.string,
  accent: types.string,
  error: types.string,
  text: types.string,
  onSurface: types.string,
  disabled: types.string,
  placeholder: types.string,
  backdrop: types.string,
  notification: types.string,
});

const animation = types.model({
  scale: types.number,
});
export const themeStore = types.model({
  dark: types.boolean,
  mode: types.optional(
    types.enumeration('fontWeight', ['adaptive', 'exact']),
    'adaptive'
  ),
  roundness: types.number,
  colors: colorStore,
  fonts: fonts,
  animation: animation,
});
