import AsyncStorage from '@react-native-async-storage/async-storage';
import {RootStoreModel} from '../index';
import {DefaultTheme} from 'react-native-paper';
import {loadData} from '../../utils/storage';
import {unprotect} from 'mobx-state-tree';
export async function setupRootStore() {
  let searchHistory = [];
  let currentTheme = DefaultTheme;
  try {
    let data = await loadData('search-history');
    if (data !== null) {
      searchHistory = data;
    }
  } catch (err) {}
  try {
    let data = await loadData('current-theme');
    if (data !== null) {
      currentTheme = data;
    }
  } catch (err) {}
  const RootStore = RootStoreModel.create({
    flickrData: [],
    searchHistory: searchHistory,
    currentTheme: currentTheme,
  });
  return RootStore;
}
