import {types} from 'mobx-state-tree';
import {flow} from 'mobx-state-tree';

import {flickrImageModel} from '../flickr-store/flickr-store';
import PhotoSearchApi from '../../api/photosearch';
import {saveData} from '../../utils/storage';
import {DefaultTheme} from 'react-native-paper';
import {themeStore} from '../theme-store/theme-store';
export const RootStoreModel = types
  .model('RootStore')
  .props({
    flickrData: types.optional(types.array(flickrImageModel), []),
    searchHistory: types.optional(types.array(types.string), []),
    currentTheme: types.optional(themeStore, DefaultTheme),
  })
  .views((self) => ({
    get data() {
      return self.flickrData;
    },
    get theme() {
      return self.currentTheme;
    },
  }))
  .actions((self) => ({
    setFlickrImage: flow(function* (value) {
      let data = yield PhotoSearchApi.getSearch(value);
      data = data.data.photos.photo;
      self.flickrData = [...data];
    }),
    addSearchHistory: flow(function* (value) {
      if (self.searchHistory.includes(value) == false) {
        let data = [value, ...self.searchHistory.slice(0, 2)];
        self.searchHistory = data;
        yield saveData('search-history', JSON.stringify(data));
      }
    }),
    deleteSearchHistory: flow(function* (value) {
      let data = self.searchHistory.filter((searchTerm) => {
        if (searchTerm === value) {
          return false;
        }
        return true;
      });
      self.searchHistory = data;
      yield saveData('search-history', JSON.stringify(data));
    }),
    setTheme: flow(function* (theme) {
      yield saveData('current-theme', JSON.stringify(theme));
      self.currentTheme = {...theme};
    }),
  }));
