import {types} from 'mobx-state-tree';
export const flickrImageModel = types.model({
  title: types.optional(types.string, ''),
  server: types.optional(types.string, ''),
  id: types.optional(types.string, ''),
  secret: types.optional(types.string, ''),
});
/*secret: types.string,
  id: types.string,
  server: types.string,
  description: types.string,
  views: types.string,*/
