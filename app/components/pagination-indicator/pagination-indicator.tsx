import React, {useEffect, useState} from 'react';
import {View, StyleSheet, Dimensions} from 'react-native';
import Animated, {
  useAnimatedStyle,
  interpolate,
  withSpring,
} from 'react-native-reanimated';
const {width} = Dimensions.get('window');

interface PaginationIndicatorProps {
  scrollPosition: {value: number};
  indicatorWidth: number;
  numberOfScreens: number;
}

export const PaginationIndicator: React.FC<PaginationIndicatorProps> = ({
  scrollPosition,
  indicatorWidth,
  numberOfScreens,
}) => {
  const firstBarStyle = useAnimatedStyle(() => {
    const translateInterPolate = interpolate(
      scrollPosition.value,
      [0, (numberOfScreens - 1) * width],
      [0, indicatorWidth - 9]
    );
    return {
      transform: [{translateX: withSpring(translateInterPolate)}],
    };
  });
  const [dots, setDots] = useState([0]);
  useEffect(() => {
    setDots(Array.from({length: numberOfScreens - 1}, () => 0));
  }, []);
  return (
    <View style={[styles.container, {width: indicatorWidth}]}>
      <View style={styles.greenDotContainer}>
        <View style={[styles.unselectedDots]} />
        <Animated.View style={[styles.selectedDots, firstBarStyle]} />
      </View>
      {dots.map((_, index) => {
        return <View key={index} style={[styles.unselectedDots]} />;
      })}
    </View>
  );
};
const styles = StyleSheet.create({
  container: {
    alignSelf: 'center',
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  unselectedDots: {
    height: 4,
    width: 9,
    borderRadius: 4,
    backgroundColor: 'grey',
  },
  selectedDots: {
    height: 4,
    width: 10,
    borderRadius: 4,
    backgroundColor: '#32de05',
    top: -4,
    elevation: 3,
  },
  greenDotContainer: {
    height: 4,
    width: 9,
  },
});
