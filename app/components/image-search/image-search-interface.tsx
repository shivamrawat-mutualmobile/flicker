interface DescriptionInterface {
  _content: string;
}
interface OwnerInterface {
  username: string;
}

export interface PhotoDetailsInterface {
  description: DescriptionInterface;
  owner: OwnerInterface;
  views: string;
}

export interface PhotoDetailInterface {
  photo: PhotoDetailsInterface;
}
export type PhotoListRowProps = {
  title: string;
  server: string;
  id: string;
  secret: string;
};
