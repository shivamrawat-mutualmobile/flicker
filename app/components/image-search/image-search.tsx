import React, {useEffect, useState} from 'react';
import {View, StyleSheet, Image, TouchableOpacity} from 'react-native';
import {Text} from 'react-native-paper';
import PhotoSearchApi from '../../api/photosearch';
import {PhotoDetailInterface, PhotoListRowProps} from './index';
import {useNavigation} from '@react-navigation/native';
import {StackNavigationProp} from '@react-navigation/stack';
import {RootStackParamList} from '../../navigation/root-navigator';
import {getPhotoUrl} from '../../utils/flickr-photo-url-constructor';

type ProfileScreenNavigationProp = StackNavigationProp<RootStackParamList>;

export function PhotoListRow({title, server, id, secret}: PhotoListRowProps) {
  const [photoDetail, setPhotoDetail] = useState<
    PhotoDetailInterface | undefined
  >(undefined);

  const fetchData = async () => {
    let data: {data: PhotoDetailInterface} = await PhotoSearchApi.getInfo(
      id,
      secret
    );
    setPhotoDetail(data.data);
  };

  useEffect(() => {
    fetchData();
  }, []);
  const navigation = useNavigation<ProfileScreenNavigationProp>();
  return (
    <TouchableOpacity
      disabled={photoDetail ? false : true}
      onPress={() => {
        navigation.navigate('PhotoDetailScreen', {
          title: title,
          server: server,
          id: id,
          secret: secret,
          photo: photoDetail?.photo,
        });
      }}>
      <View style={styles.container}>
        <Image
          source={{
            uri: getPhotoUrl(server, id, secret, 'w'),
          }}
          style={styles.imageStyle}
        />
        <View style={styles.innerContainer}>
          <Text style={styles.title}>{title}</Text>
          {photoDetail ? (
            <View style={styles.infocontainer}>
              <Text style={styles.description}>
                Username ○ {photoDetail.photo.owner.username}
              </Text>
              <Text style={styles.description}>
                👁️ {photoDetail.photo.views}
              </Text>
            </View>
          ) : null}
        </View>
      </View>
    </TouchableOpacity>
  );
}
const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    marginVertical: 20,
    paddingHorizontal: 10,
  },
  infocontainer: {},
  imageStyle: {
    width: 120,
    height: 120,
  },
  title: {
    fontSize: 15,
    paddingBottom: 10,
  },
  description: {},
  innerContainer: {
    paddingLeft: 20,
    flex: 1,
  },
});
