import React from 'react';
import {View} from 'react-native';
import {Searchbar} from 'react-native-paper';

type SearchProps = {
  value: string;
  onChangeText: (value: string) => void;
  onEndEditing: () => void;
  onFocus: () => void;
};

export const SearchBarComponent = ({
  value,
  onChangeText,
  onEndEditing,
  onFocus,
}: SearchProps) => {
  return (
    <View>
      <Searchbar
        placeholder="Search"
        onChangeText={onChangeText}
        value={value}
        onEndEditing={onEndEditing}
        onTouchStart={onFocus}
      />
    </View>
  );
};
