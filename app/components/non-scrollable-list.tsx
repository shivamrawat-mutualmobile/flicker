import React from 'react';
import {View, StyleSheet, TouchableOpacity} from 'react-native';
import {Text} from 'react-native-paper';
import Icon from 'react-native-vector-icons/Entypo';
interface flatListItem {
  item: {title: string; secret: string; server: string; id: string};
}
interface NonScrollableListProps {
  data: string[];
  enabled: boolean;
  onPressList: (searchterm: string) => void;
  deleteHistory: (searchterm: string) => void;
}

export const NonScrollableList: React.FC<NonScrollableListProps> = ({
  data,
  enabled,
  onPressList,
  deleteHistory,
}) => {
  return (
    <>
      {data && enabled
        ? data.map((value, index) => {
            return (
              <TouchableOpacity
                key={index}
                onPress={() => {
                  onPressList(value);
                }}>
                <View style={styles.listcontainer}>
                  <Text>{value}</Text>
                  <TouchableOpacity
                    onPress={() => {
                      deleteHistory(value);
                    }}>
                    <Icon style={styles.icon} name="circle-with-cross" />
                  </TouchableOpacity>
                </View>
              </TouchableOpacity>
            );
          })
        : null}
    </>
  );
};

const styles = StyleSheet.create({
  listcontainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginLeft: 50,
    marginVertical: 10,
    flexGrow: 0,
  },
  icon: {
    paddingRight: 20,
    flex: 1,
  },
});
