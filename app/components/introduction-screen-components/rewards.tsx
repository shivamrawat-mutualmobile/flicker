/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * Generated with the TypeScript template
 * https://github.com/react-native-community/react-native-template-typescript
 *
 * @format
 */

import React from 'react';
import {StyleSheet, View, Dimensions} from 'react-native';
import {Text, useTheme} from 'react-native-paper';
const {width, height} = Dimensions.get('window');
import Animated, {useAnimatedStyle, withSpring} from 'react-native-reanimated';
interface ScreenProps {
  transformX: any;
}
export const RewardsComponent: React.FC<ScreenProps> = ({transformX}) => {
  const {colors} = useTheme();
  const titleStyle = useAnimatedStyle(() => {
    const transformValue = transformX.value - width;
    return {
      transform: [{translateX: withSpring(-transformValue)}],
    };
  });
  const descriptionStyle = useAnimatedStyle(() => {
    const transformValue = transformX.value - width;
    return {
      transform: [{translateX: withSpring(-transformValue / 2)}],
      opacity: 1 - (transformValue / width) * 2,
    };
  });
  return (
    <View style={styles.container}>
      <View style={styles.imageContainer}>
        <Text>Image</Text>
      </View>
      <View style={styles.titleContainer}>
        <Animated.Text style={[styles.title, titleStyle, {color: colors.text}]}>
          Collect Sign in Rewards !
        </Animated.Text>
        <Animated.Text
          style={[styles.description, descriptionStyle, {color: colors.text}]}>
          This is the text that does not mean anything and can be considered a
          placeholder text.
        </Animated.Text>
      </View>
    </View>
  );
};
const styles = StyleSheet.create({
  container: {
    width: width,
    height: height - 200,
    alignItems: 'center',
  },
  imageContainer: {
    height: 300,
  },
  titleContainer: {
    marginTop: 20,
    alignItems: 'center',
    width: 300,
  },
  title: {
    fontSize: 20,
  },
  description: {
    fontSize: 14,
    paddingTop: 20,
    textAlign: 'center',
  },
});
