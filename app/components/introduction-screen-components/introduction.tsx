/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * Generated with the TypeScript template
 * https://github.com/react-native-community/react-native-template-typescript
 *
 * @format
 */

import React from 'react';
import {StyleSheet, View, Dimensions} from 'react-native';
const {width, height} = Dimensions.get('window');
import Animated, {
  color,
  useAnimatedStyle,
  withSpring,
} from 'react-native-reanimated';
import {Text, useTheme} from 'react-native-paper';

interface ScreenProps {
  transformX: any;
}
export const IntroductionComponent: React.FC<ScreenProps> = ({transformX}) => {
  const {colors} = useTheme();
  const titleStyle = useAnimatedStyle(() => {
    return {
      transform: [{translateX: withSpring(-transformX.value)}],
    };
  });
  const descriptionStyle = useAnimatedStyle(() => {
    return {
      transform: [{translateX: withSpring(-transformX.value / 2)}],
      opacity: 1 - (transformX.value / width) * 2,
    };
  });
  return (
    <View style={styles.container}>
      <View style={styles.imageContainer}>
        <Text>Image</Text>
      </View>
      <View style={styles.titleContainer}>
        <Animated.Text style={[styles.title, titleStyle, {color: colors.text}]}>
          Welcome
        </Animated.Text>
        <Animated.Text
          style={[styles.description, descriptionStyle, {color: colors.text}]}>
          This is the welcome screen and this is the test text and does not mean
          anything.
        </Animated.Text>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    width: width,
    height: height - 200,
    alignItems: 'center',
  },
  imageContainer: {
    height: 300,
  },
  titleContainer: {
    marginTop: 20,
    alignItems: 'center',
    width: 300,
  },
  title: {
    fontSize: 20,
  },
  description: {
    fontSize: 14,
    paddingTop: 20,
    textAlign: 'center',
  },
});
