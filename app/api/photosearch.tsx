import {Api} from './api';
import {APIKEY} from '../config/env';
class PhotoSearch extends Api {
  constructor() {
    super();
  }
  async getSearch(text: string) {
    const param = {
      method: 'flickr.photos.search',
      api_key: APIKEY,
      text: text,
      per_page: 10,
      format: 'json',
      nojsoncallback: 1,
    };
    let data = await this.get(param);
    return data;
  }
  async getInfo(id: string, secret: string) {
    const param = {
      method: 'flickr.photos.getInfo',
      secret: secret,
      photo_id: id,
      api_key: APIKEY,
      format: 'json',
      nojsoncallback: 1,
    };
    let data = await this.get(param);
    return data;
  }
}

const PhotoSearchApi = new PhotoSearch();
export default PhotoSearchApi;
