import {ApisauceInstance, create} from 'apisauce';
import {ApiConfig, DEFAULT_API_CONFIG} from './api-config';

interface APIParameters {
  method: string;
  api_key: string;
  format: string;
  nojsoncallback: number;
}
export interface GetPhotoParameters extends APIParameters {
  text: string;
  per_page: number;
}
export interface PostPhotoParameters extends APIParameters {
  secret: string;
  photo_id: string;
}
export class Api {
  apicreate: ApisauceInstance;
  constructor(config: ApiConfig = DEFAULT_API_CONFIG) {
    this.apicreate = create(config);
  }

  async get(parameters: GetPhotoParameters | PostPhotoParameters) {
    let data = await this.apicreate.get('/', parameters);
    return data;
  }
}
