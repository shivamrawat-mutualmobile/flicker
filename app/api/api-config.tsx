export interface ApiConfig {
  baseURL: string;
  timeout: number;
  headers: {
    Accept: string;
  };
}

export const DEFAULT_API_CONFIG = {
  baseURL: 'https://www.flickr.com/services/rest',
  timeout: 2000,
  headers: {
    Accept: 'application/json',
  },
};
