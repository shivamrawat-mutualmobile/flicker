import React from 'react';
import {StyleSheet, Button, Dimensions, View} from 'react-native';
import Animated, {
  useAnimatedScrollHandler,
  useSharedValue,
} from 'react-native-reanimated';
import {PaginationIndicator} from '../../components/pagination-indicator/pagination-indicator';
import {
  IntroductionComponent,
  OffersComponent,
  RewardsComponent,
} from '../../components/introduction-screen-components/index';
import {RootStackParamList} from '../../navigation/root-navigator';
import {StackNavigationProp} from '@react-navigation/stack';

type IntroductionScreenProps = StackNavigationProp<
  RootStackParamList,
  'IntroductionScreen'
>;
export const IntroductionScreen: React.FC<IntroductionScreenProps> = ({
  navigation,
}) => {
  const scrollPosition = useSharedValue<number>(0);
  const scrollEvent = useAnimatedScrollHandler({
    onScroll: (e) => {
      scrollPosition.value = e.contentOffset.x;
    },
  });

  return (
    <View>
      <Animated.ScrollView
        horizontal
        pagingEnabled
        showsHorizontalScrollIndicator={false}
        onScroll={scrollEvent}
        scrollEventThrottle={16}>
        <IntroductionComponent transformX={scrollPosition} />
        <RewardsComponent transformX={scrollPosition} />
        <OffersComponent transformX={scrollPosition} />
      </Animated.ScrollView>
      <View style={styles.paginationContainer}>
        <PaginationIndicator
          scrollPosition={scrollPosition}
          indicatorWidth={120}
          numberOfScreens={3}
        />
      </View>
      <Button
        title="Continue"
        onPress={() => {
          navigation.navigate('PhotoListScreen');
        }}
      />
      <Button
        title="Push"
        onPress={() => {
          navigation.push('PhotoListScreen');
        }}
      />
      <Button
        title="Replace"
        onPress={() => {
          navigation.replace('PhotoListScreen');
        }}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  paginationContainer: {
    alignItems: 'center',
    flex: 1,
    position: 'absolute',
    top: 300,
    left: 0,
    right: 0,
  },
});
