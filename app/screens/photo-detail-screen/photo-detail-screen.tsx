import React from 'react';
import {View, StyleSheet, Image, ScrollView} from 'react-native';
import {Button, Text, DefaultTheme} from 'react-native-paper';
import {
  PhotoDetailsInterface,
  PhotoListRowProps,
} from '../../components/image-search/image-search-interface';
import {getPhotoUrl} from '../../utils/flickr-photo-url-constructor';
import {useStores} from '../../model';
import DarkThemeFonts from '../../utils/themes';
type PhotoDetailProps = {
  route: {
    params: PhotoListRowProps & PhotoDetailsInterface;
  };
};

export const PhotoDetailScreen: React.FC<PhotoDetailProps> = ({route}) => {
  const {title, server, id, secret, photo} = route.params;
  const rootStore = useStores();
  return (
    <ScrollView contentContainerStyle={styles.container}>
      <Text style={styles.title}>{title}</Text>
      <Image
        source={{
          uri: getPhotoUrl(server, id, secret, 'c'),
        }}
        style={styles.imageStyle}
      />
      <Button
        icon="keyboard-tab"
        mode="contained"
        onPress={() => rootStore.setTheme(DarkThemeFonts)}>
        DARK THEME
      </Button>
      <Button
        icon="cancel"
        mode="contained"
        onPress={() => rootStore.setTheme(DefaultTheme)}>
        LIGHT THEME
      </Button>
      {photo ? (
        <View style={styles.innerContainer}>
          <View style={styles.infocontainer}>
            <Text style={styles.description}>{photo.description._content}</Text>
            <Text style={styles.description}>
              Username ○ {photo.owner.username}
            </Text>
            <Text style={styles.description}>👁️ {photo.views}</Text>
          </View>
        </View>
      ) : null}
    </ScrollView>
  );
};
const styles = StyleSheet.create({
  container: {
    paddingBottom: 20,
  },
  title: {
    fontSize: 25,
    fontWeight: 'bold',
    paddingVertical: 30,
    marginHorizontal: 20,
    alignSelf: 'center',
  },
  description: {
    paddingVertical: 20,
  },
  imageStyle: {
    width: 300,
    height: 260,
    borderWidth: 2,
    borderColor: 'black',
    alignSelf: 'center',
    borderRadius: 20,
  },
  infocontainer: {
    marginHorizontal: 10,
  },
  innerContainer: {
    paddingLeft: 20,
  },
});
