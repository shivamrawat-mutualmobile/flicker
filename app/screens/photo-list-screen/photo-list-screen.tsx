import React, {useState} from 'react';
import {View, StyleSheet, FlatList} from 'react-native';
import {PhotoListRow} from '../../components/image-search/image-search';
import {SearchBarComponent} from '../../components/search-bar';
import {useStores} from '../../model';
import {observer} from 'mobx-react-lite';
import {NonScrollableList} from '../../components/non-scrollable-list';
import {useTheme} from 'react-native-paper';
interface flatListItem {
  item: {title: string; secret: string; server: string; id: string};
}
interface PhotoListProps {}
export const PhotoListScreen: React.FC<PhotoListProps> = observer(() => {
  const [value, setValue] = useState('');
  const rootStore = useStores();
  const {colors} = useTheme();
  const [history, setHistory] = useState(false);
  async function endEditing(searchterm: string = value) {
    setHistory(false);
    await rootStore.setFlickrImage(searchterm);
    if (searchterm === value) {
      await rootStore.addSearchHistory(searchterm);
    }
    setValue(searchterm);
    //setFlatlistdata(rootStore.data);
  }
  const onFocus = () => {
    setHistory(true);
  };
  const deleteHistory = async (value) => {
    await rootStore.deleteSearchHistory(value);
  };
  return (
    <View style={styles.container}>
      <SearchBarComponent
        value={value}
        onChangeText={setValue}
        onEndEditing={() => {
          endEditing();
        }}
        onFocus={onFocus}
      />
      <NonScrollableList
        data={rootStore ? rootStore.searchHistory.slice() : []}
        enabled={history}
        onPressList={endEditing}
        deleteHistory={deleteHistory}
      />
      <FlatList
        style={{backgroundColor: colors.background}}
        data={rootStore ? rootStore.flickrData.slice() : []}
        renderItem={({item}: flatListItem) => {
          return (
            <PhotoListRow
              title={item.title}
              secret={item.secret}
              server={item.server}
              id={item.id}
            />
          );
        }}
      />
    </View>
  );
});
const styles = StyleSheet.create({
  container: {},
  listcontainer: {
    marginLeft: 50,
    marginVertical: 10,
    flexGrow: 0,
  },
});
